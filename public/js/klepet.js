var Klepet = function(socket) {
  this.socket = socket;
};

Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};

Klepet.prototype.spremeniKanal = function(kanal, geslo) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal,
    geslo: geslo
  });
};

Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      besede = besede.join(' ');
      besede = besede.split('"');
      if(besede.length === 1){
        this.spremeniKanal(besede[0].trim(), "");
      }
      else if(besede.length >= 3){
        besede.shift();
        this.spremeniKanal(besede[0].trim(), besede[2].trim());
      }
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      besede.shift();
      var besedisce= besede.join(' ');
      besedisce = besedisce.split('" ');
      var vzdevekPrejemnika = besedisce[0].replace('"','');
      besedisce[1] = besedisce[1].split('"');
      besedisce[1].shift();
      besedisce[1].pop();
      besedisce[1] = besedisce[1].join('"');
      //besedisce[1] = besedisce[1].replace(/</g , "&lt");
      //besedisce[1] = besedisce[1].replace(/>/g , "&gt");
      var sporociloPrejemnika = besedisce[1]; 
      this.socket.emit('zasebnoSporocilo', vzdevekPrejemnika,sporociloPrejemnika);
      break;
      
    default:
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};
