var besede;
var trenutniKanal = {};
var globalUrlOfEmoticans = "https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/";
var specificEmoticans = [
    [/\;\)/g, "wink.png"],
    [/\:\)/g, "smiley.png"],
    [/\(y\)/g, "like.png"],
    [/\:\*/g, "kiss.png"],
    [/\:\(/g, "sad.png"],
  ];
function changeMessage(sporocilo){
  for (var i = 0; i < specificEmoticans.length; i++) {
    sporocilo = sporocilo.replace(specificEmoticans[i][0], '<img src="' + globalUrlOfEmoticans + specificEmoticans[i][1] + '"/>');
  }
  return sporocilo;
}
$(document).ready(function() {
    $.ajax({type: "GET", url:"swearWords.xml",dataType:"xml",
      success:function(xml){
        $(xml).find('words').each(function(){
          besede = $(xml).find("word").map(function() {
            return this.innerHTML;
          }).get();
          for (var i = 0; i < besede.length; i++) {
            besede[i] = new RegExp( "\\b" +besede[i] + "\\b","g");
          }
        });
      }
      
    });
});
function zamenjava(beseda){
  var a = "";
  beseda = beseda.replace("/\\b", "").replace("\\b/g", "");
  
  for (var i = 0; i < beseda.length; i++) {
    if(beseda.charAt(i) !== " "){
      a +=  "*";
    }
    else{
      a += " ";
    }
  }
    return a;
}
  
function grdeBeseda(sprocilo){
  for (var i = 0; i < besede.length; i++) {
    sprocilo = sprocilo.replace( besede[i], zamenjava(besede[i].toString()));
  }
  return sprocilo;
}

function divElementEnostavniTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').html(sporocilo);
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}
function spremeniSeznamUporabnikov(sporocilo){
  $('#seznam-uporabnikov').empty();
     var ljudjeNaKanalu = sporocilo.split(',');
    for (var i in ljudjeNaKanalu) {
      $('#seznam-uporabnikov').append(divElementEnostavniTekst(ljudjeNaKanalu[i]));
    }
}

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;
  if (sporocilo.charAt(0) == '/') {
    sporocilo = sporocilo.replace(/</g , "&lt");
    sporocilo = sporocilo.replace(/>/g , "&gt");
    sporocilo = changeMessage(sporocilo);
    sporocilo = grdeBeseda(sporocilo);
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } 
  else {
    sporocilo = sporocilo.replace(/</g , "&lt");
    sporocilo = sporocilo.replace(/>/g , "&gt");
    sporocilo = changeMessage(sporocilo);
    sporocilo = grdeBeseda(sporocilo);
    klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

var socket = io.connect();

$(document).ready(function() {
  var klepetApp = new Klepet(socket);

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      $('#kanal').text(rezultat.vzdevek + " @ " + rezultat.kanal );
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
      
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
    if(rezultat.uspesno){
      spremeniSeznamUporabnikov(rezultat.naKanalu);
    }
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal = rezultat.kanal;
    $('#kanal').text(rezultat.vzdevek + " @ " + rezultat.kanal );
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
    spremeniSeznamUporabnikov(rezultat.naKanalu);
  });

  socket.on('sporocilo', function (sporocilo) {
    //var novElement = $('<div style="font-weight: bold"></div>').html(sporocilo.besedilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo.besedilo));
     spremeniSeznamUporabnikov(sporocilo.naKanalu);
  });
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }
    
    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });

  setInterval(function() {
    socket.emit('kanali');
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});