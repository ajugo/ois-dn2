var socketio = require('socket.io');
var io;
var stevilkaGosta = 1;
var vzdevkiGledeNaSocket = {};
var uporabljeniVzdevki = [];
var trenutniKanal = {};
var shranjenoGeslo = {};
exports.listen = function(streznik) {
  io = socketio.listen(streznik);
  io.set('log level', 1);
  io.sockets.on('connection', function (socket) {
    stevilkaGosta = dodeliVzdevekGostu(socket, stevilkaGosta, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    pridruzitevKanalu(socket, 'Skedenj', '');
    obdelajPosredovanjeSporocila(socket, vzdevkiGledeNaSocket);
    obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
    obdelajPridruzitevKanalu(socket);
    obdelajZasebnoSporocilo(socket);
    socket.on('kanali', function() {
      socket.emit('kanali', io.sockets.manager.rooms);
    });
    obdelajOdjavoUporabnika(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki);
  });
};
function uporabnikiKanala(socket, kanal){
  var naKanalu = '';
  var uporabnikiNaKanalu = io.sockets.clients(kanal);
  
  for (var i in uporabnikiNaKanalu) {
    var uporabnikSocketId = uporabnikiNaKanalu[i].id;
    if (i > 0) {
       naKanalu += ',';
    }
    naKanalu += vzdevkiGledeNaSocket[uporabnikSocketId];
  }
  
  return naKanalu;
    
}
function dodeliVzdevekGostu(socket, stGosta, vzdevki, uporabljeniVzdevki) {
  var vzdevek = 'Gost' + stGosta;
  vzdevki[socket.id] = vzdevek;
  socket.emit('vzdevekSpremembaOdgovor', {
    uspesno: true,
    vzdevek: vzdevek,
    naKanalu: uporabnikiKanala(socket,trenutniKanal[socket.id])
  });
  uporabljeniVzdevki.push(vzdevek);
  return stGosta + 1;
}

function pridruzitevKanalu(socket, kanal, geslo) {
  socket.join(kanal);
  trenutniKanal[socket.id] = kanal;
  shranjenoGeslo[socket.id] = geslo;
  socket.emit('pridruzitevOdgovor', {kanal: kanal, vzdevek:vzdevkiGledeNaSocket[socket.id], naKanalu:uporabnikiKanala(socket,kanal)});
  socket.broadcast.to(kanal).emit('sporocilo', {
    besedilo: vzdevkiGledeNaSocket[socket.id] + ' se je pridružil kanalu ' + kanal + '.',
    naKanalu:uporabnikiKanala(socket,kanal)
  });

  var uporabnikiNaKanalu = io.sockets.clients(kanal);
  if (uporabnikiNaKanalu.length > 1) {
    var uporabnikiNaKanaluPovzetek = 'Trenutni uporabniki na kanalu ' + kanal + ': ';
    for (var i in uporabnikiNaKanalu) {
      var uporabnikSocketId = uporabnikiNaKanalu[i].id;
      if (uporabnikSocketId != socket.id) {
        if (i > 0) {
          uporabnikiNaKanaluPovzetek += ', ';

        }
        uporabnikiNaKanaluPovzetek += vzdevkiGledeNaSocket[uporabnikSocketId];
      }
    }
    uporabnikiNaKanaluPovzetek += '.';
    socket.emit('sporocilo', {besedilo: uporabnikiNaKanaluPovzetek, naKanalu:uporabnikiKanala(socket,kanal)});
  }
}

function obdelajZahtevoZaSprememboVzdevka(socket, vzdevkiGledeNaSocket, uporabljeniVzdevki) {
  socket.on('vzdevekSpremembaZahteva', function(vzdevek) {
    if (vzdevek.indexOf('Gost') == 0) {
      socket.emit('vzdevekSpremembaOdgovor', {
        uspesno: false,
        sporocilo: 'Vzdevki se ne morejo začeti z "Gost".'
      });
    } else {
      if (uporabljeniVzdevki.indexOf(vzdevek) == -1) {
        var prejsnjiVzdevek = vzdevkiGledeNaSocket[socket.id];
        var prejsnjiVzdevekIndeks = uporabljeniVzdevki.indexOf(prejsnjiVzdevek);
        uporabljeniVzdevki.push(vzdevek);
        vzdevkiGledeNaSocket[socket.id] = vzdevek;
        delete uporabljeniVzdevki[prejsnjiVzdevekIndeks];
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: true,
          vzdevek: vzdevek,
          kanal: trenutniKanal[socket.id],
          naKanalu: uporabnikiKanala(socket,trenutniKanal[socket.id])
        });
        socket.broadcast.to(trenutniKanal[socket.id]).emit('sporocilo', {
          besedilo: prejsnjiVzdevek + ' se je preimenoval v ' + vzdevek + '.',
          naKanalu: uporabnikiKanala(socket,trenutniKanal[socket.id])
        });
      } else {
        socket.emit('vzdevekSpremembaOdgovor', {
          uspesno: false,
          sporocilo: 'Vzdevek je že v uporabi.'
        });
      }
    }
  });
}

function obdelajPosredovanjeSporocila(socket) {
  socket.on('sporocilo', function (sporocilo) {
    socket.broadcast.to(sporocilo.kanal).emit('sporocilo',{
      besedilo: vzdevkiGledeNaSocket[socket.id] + ': ' + sporocilo.besedilo,
    });
  });
}

function obdelajPridruzitevKanalu(socket) {
  socket.on('pridruzitevZahteva', function(kanal) {
    var preveri = false;
    //console.log( kanal.novKanal+ "-" + kanal.geslo);
    for(var i in trenutniKanal){
      
      if(trenutniKanal[i] === kanal.novKanal && shranjenoGeslo[i] === kanal.geslo){
        //console.log(1);
        socket.leave(trenutniKanal[socket.id]);
        delete trenutniKanal[socket.id];
        delete shranjenoGeslo[socket.id];
        socket.broadcast.to(trenutniKanal[socket.id]).emit('sporocilo',{besedilo:"", naKanalu: uporabnikiKanala(socket,trenutniKanal[socket.id])});
        pridruzitevKanalu(socket, kanal.novKanal, kanal.geslo);
        preveri = true;
        break;
      }
      else if(trenutniKanal[i] === kanal.novKanal && shranjenoGeslo[i] === "" && kanal.geslo !== ""){
        //console.log(2);
        socket.emit('sporocilo', {
          besedilo: "Izbrani kanal " +  kanal.novKanal +" je prosto dostopen in ne zahteva prijave z geslom, zato se prijavite z uporabo /pridruzitev " + kanal.novKanal +" ali zahtevajte kreiranje kanala z drugim imenom.",
          naKanalu: uporabnikiKanala(socket,trenutniKanal[socket.id])
        });
        preveri = true;
        break;
      }
      else if(trenutniKanal[i] === kanal.novKanal && shranjenoGeslo[i] !== kanal.geslo){
       // console.log(3);
        socket.emit('sporocilo', {
          besedilo: "Pridružitev v kanal " +  kanal.novKanal +" ni bilo uspešno, ker je geslo napačno!",
          naKanalu: uporabnikiKanala(socket,trenutniKanal[socket.id])
        });
        preveri = true;
        break;
      }
    }
    
    if(preveri === false){
      //console.log(4);
      socket.leave(trenutniKanal[socket.id]);
      socket.broadcast.to(trenutniKanal[socket.id]).emit('sporocilo',{besedilo:"", naKanalu: uporabnikiKanala(socket,trenutniKanal[socket.id])});
      pridruzitevKanalu(socket, kanal.novKanal, kanal.geslo);
    }
  });
}
function obdelajZasebnoSporocilo(socket){
  socket.on('zasebnoSporocilo',function(vzdevekPrejemnika,sporociloPrejemnika){
  var kanali = io.sockets.manager.rooms;
  var ena = 0;
  var poslano = false;
  //console.log(kanali);
  for (var kanal in kanali) {
    kanal = kanal.substring(0, kanal.length);
    if(kanal !== ''){
      var uporabnikiNaKanalu = io.sockets.clients(trenutniKanal[kanal.id]);
      //console.log(uporabnikiNaKanalu);
      if (uporabnikiNaKanalu.length > 1) {
        for (var i in uporabnikiNaKanalu) {
          var uporabnikSocketId = uporabnikiNaKanalu[i].id;
          if (uporabnikSocketId != socket.id) {
            if (vzdevkiGledeNaSocket[uporabnikSocketId] === vzdevekPrejemnika){
              uporabnikiNaKanalu[i].emit('sporocilo', {besedilo: vzdevkiGledeNaSocket[socket.id] + "(zasebno):" + sporociloPrejemnika,naKanalu: uporabnikiKanala(socket,trenutniKanal[uporabnikiNaKanalu[i].id])});
              ena += 1;
              poslano = true;
            }
          }
        }
        if(ena === 1)
          break;
      }
    }
    if(ena === 1)
      break;
  }
    if(poslano === false)
      socket.emit('sporocilo', {besedilo: "Sporočila \"" +sporociloPrejemnika + "\" uporabniku z vzdevkom \"" + vzdevekPrejemnika + "\" ni bilo mogoče posredovati.", naKanalu: uporabnikiKanala(socket,trenutniKanal[socket.id])});
    else
      socket.emit('sporocilo', {besedilo:"(Zasebno za " + vzdevekPrejemnika + "):" + sporociloPrejemnika, naKanalu: uporabnikiKanala(socket,trenutniKanal[socket.id])});
  });
}
function obdelajOdjavoUporabnika(socket) {
  socket.on('odjava', function() {
    var vzdevekIndeks = uporabljeniVzdevki.indexOf(vzdevkiGledeNaSocket[socket.id]);
    delete uporabljeniVzdevki[vzdevekIndeks];
    delete vzdevkiGledeNaSocket[socket.id];
  });
}